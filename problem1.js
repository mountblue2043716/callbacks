function createAndDelete(directoryPath) {
    // create a new directory
    const fs = require('fs');
    fs.mkdir(directoryPath, (err) => {
        if (err) {
            console.error(err);
        } else {
            console.log('Directory created successfully.');
        }
    });

    // write random files to directory
    let filesCount = 3;
    for (let index = 1; index <= filesCount; index++) {
        obj = { element: index, square: index * index };
        fs.writeFile(`${directoryPath}/file${index}.json`, JSON.stringify(obj), (err) => {
            if (err) {
                console.error(err);
                return;
            } else {
                console.log(`${directoryPath}/file${index}.json created successfully.`);
            }
            if (filesCount == 3) {
                //delete the files simultaneously.
                const filenames = fs.readdirSync(directoryPath);
                filenames.forEach((filename) => {
                    const filePath = directoryPath + '/' + filename;
                    fs.unlink(filePath, (err) => {
                        if (err) throw err;
                        console.log(`Deleted file: ${filename}`);
                    });
                });
            }
        });
    }
}
module.exports = createAndDelete;

