

function fileManipulation(path) {
    // 1. Read file.
    const fs = require('fs');
    fs.readFile(path, 'utf-8', (err, data) => {
        if (err) throw err;

        //2. 2. Convert the content to uppercase & write to a new file. 
        //Store the name of the new file in filenames.txt
        textInUpperCase = data.toUpperCase();
        fs.writeFile(`./text-in-upper-case.txt`, textInUpperCase, (err) => {
            if (err) {
                console.error(err);
                return;
            }
            fs.writeFile('./filenames.txt', 'text-in-upper-case.txt\n', (err) => {
                if (err) {
                    console.error(err);
                    return;
                }
                //3. Read the new file and convert it to lower case. Then split the contents into sentences. 
                //Then write it to a new file. Store the name of the new file in filenames.txt
                fs.readFile('./text-in-upper-case.txt', 'utf-8', function (err, data) {
                    if (err) throw err;
                    let textInLowerCase = data.toLowerCase();
                    let splittedText = textInLowerCase.split('.').join('.\n');
                    fs.writeFile(`./splitted-text.txt`, splittedText, (err) => {
                        if (err) {
                            console.error(err);
                            return;
                        }
                        fs.writeFile(`./filenames.txt`, 'splitted-text.txt\n', { flag: 'a' }, (err) => {
                            if (err) {
                                console.error(err);
                                return;
                            }
                            //4. Read the new files, sort the content, write it out to a new file. 
                            //Store the name of the new file in filenames.txt
                            fs.readFile('./splitted-text.txt', 'utf-8', function (err, data) {
                                if (err) throw err;
                                let sortedText = data.split('.').sort();
                                fs.writeFile(`./sorted-text.txt`, sortedText, (err) => {
                                    if (err) {
                                        console.error(err);
                                        return;
                                    }
                                    fs.writeFile(`./filenames.txt`, 'sorted-text.txt\n', { flag: 'a' }, (err) => {
                                        if (err) {
                                            console.error(err);
                                            return;
                                        }
                                        //5. Read the contents of filenames.txt and delete all the new files that are 
                                        //mentioned in that list simultaneously.  
                                        fs.readFile('./filenames.txt', 'utf8', function (err, data) {
                                            if (err) throw err;
                                            // console.log(data);
                                            fileNamesList = data.split('\n');
                                            fileNamesList = fileNamesList.filter((fileName) => fileName !== '');
                                            // console.log(fileNamesList)
                                            fileNamesList.forEach((file) => {
                                                // const filePath = directoryPath + '/' + file;
                                                fs.unlink(file, (err) => {
                                                    if (err) {
                                                        console.error(err);
                                                    } else {
                                                        console.log(`${file} deleted successfully.`);
                                                    }
                                                });
                                            });
                                        });
                                    });
                                });
                            });
                        });
                    });
                });
            });
        });
    });
}
module.exports = fileManipulation;

